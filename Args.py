import models

"""
graph_saint model :
edge_weight = True
use_normalization = True
hidden_channels = 256
lr = 0.001

cluster_gcn model :
hidden_channels = 128
lr = 0.005

graph_saint_rw, forest_fire
num_workers = 4
sample_coverage = 100
num_steps

graph_saint_rw
walk_length=2

cluster_gcn_sampler
num_workers = 12
num_parts

forest_fire
p
connectivity

"Cora", "CiteSeer", "pubmed" :
hidden_channels = 32

"Cora" :
num_val = 500
num_test = 500

Flick
batch_size = 6000
num_steps = 5

Reddit
num_parts = 1500
batch_size = 20
test_loader = True
test_batch_size = 1024

"""





class Args :
    def __init__(self, dataset='Cora', model='graph_saint', hidden_channels=32, lr=0.001, use_cuda=True, epochs=50,
                        edge_weight=True, use_normalization=True,
                        num_val=500, num_test=500,
                        loader='forest_fire', batch_size=200, num_workers=4, test_loader=False, **kwargs):
        self.dataset = dataset
        self.edge_weight = edge_weight 
        
        self.model = model
        if model == "graph_saint" or model == "saint_gcn":
            assert edge_weight == True
            self.model_class = models.GraphSaintPyGNet if model == "graph_saint" else  models.GraphSaintGCN  
            self.use_normalization = use_normalization
        elif model == "cluster_gcn" :
            self.model_class = models.ClusterGCNPyGNet
        elif model == "gat" :
            assert "heads" in kwargs
            self.model_class = models.GAT
        elif model == "gcn" :
            self.model_class = models.GCN
        
        self.hidden_channels = hidden_channels
        self.lr = lr
        self.epochs = epochs
        
        if num_val and num_test :
            self.num_val = num_val
            self.num_test = num_test
            
        self.use_cuda = use_cuda
        self.loader = loader
        self.batch_size = batch_size
        self.num_workers = num_workers
        if loader == "forest_fire" :
            assert "num_steps" in kwargs
            assert "p" in kwargs
            assert "connectivity" in kwargs
            assert "sample_coverage" in kwargs
            self.num_steps = kwargs["num_steps"]
            self.p = kwargs["p"]
            self.connectivity = kwargs["connectivity"]
            self.sample_coverage = kwargs["sample_coverage"]
        elif loader == "graph_saint_rw" :
            assert "num_steps" in kwargs
            assert "walk_length" in kwargs
            assert "sample_coverage" in kwargs
            self.num_steps = kwargs["num_steps"]
            self.walk_length = kwargs["walk_length"]
            self.sample_coverage = kwargs["sample_coverage"]
        elif loader == "cluster_gcn" :
            assert "num_parts" in kwargs
            self.num_parts = kwargs["num_parts"]
        
        self.test_loader = test_loader
        if test_loader :
            assert "test_batch_size" in kwargs 
            self.test_batch_size = kwargs["test_batch_size"]
        
        self.kwargs = kwargs
        
        
